import React from "react";
import * as Survey from "survey-react";
import "survey-react/survey.css";
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { css } from 'glamor'
import PageTitle from "../components/common/PageTitle";
import config from './config.json'
import {isMobile, isTablet} from 'react-device-detect';
import { Dimmer, Loader } from 'semantic-ui-react'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Badge,
  Button,
  Form, FormInput, FormGroup, FormCheckbox, Modal, ModalHeader, ModalBody, Alert
} from "shards-react";
class SurveyResponse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model:  {},
      publishView: false,
      currentQuestion: 0,
      start: new Date().getTime(),
      mobile: isMobile,
      tablet: isTablet
    }
    console.log("loading sr")
  }

componentDidMount = async () => {
       // console.log(window.location.pathname.substring(17))
       this.setState({loading: true})
       let load_model = await axios.post(config.url + "/respondent/survey", {
        surveyid: 1
      })
       if(load_model.data.msg == "success"){
        this.setState({model: load_model.data.model["published_model"]})
        this.setState({loading: false})

      }
  // let local_model = localStorage.getItem("model")
  //   console.log("local cduu", local_model)
  //   let parsed = JSON.parse(local_model)
  //   if(local_model != "" && local_model != null && local_model != undefined && local_model != "{}" && JSON.stringify(this.state.model) != local_model){
  //     console.log("setting stored state", parsed, this.state.model)
  //     this.setState({model: parsed})
  //   }
 
    
}

  render() {

    var model = new Survey.Model(this.state.model);
    Survey.surveyStrings.emptySurvey = "The current survey is empty";
    model.onComplete.add(async (sender, options) => {
      let end = new Date().getTime()
      let time_taken = end - this.state.start
     //Show message about "Saving..." the results
      options.showDataSaving();//you may pass a text parameter to show your own text
      console.log(sender.data)
      // let response = await axios.post(config.url + '/response/' + "1", {
      //   response: sender.data
      // })
      // var xhr = new XMLHttpRequest();
      // xhr.open("POST", "YourServiceForStoringSurveyResultsURL");
      // xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
      // xhr.onload = xhr.onerror = function () {
      // if (xhr.status == 200) {
      options.showDataSavingSuccess(); //you may pass a text parameter to show your own text
      // //Or you may clear all messages
      // //options.showDataSavingClear();
      // } else {
      // //Error
      // options.showDataSavingError(); //you may pass a text parameter to show your own text
      // }
      // };
      // xhr.send(JSON.stringify(sender.data));
      });

    return (
      <Container fluid className="main-content-container px-4" >
        {/* Page Header */}
        <Row noGutters className="page-header py-4" style={{width: "100%", clear: "both", margin: "0  auto"}}>
          <PageTitle sm="4" title="Title" subtitle="Survey" className="text-sm-left" style={{marginLeft: "10%"}}/>
        </Row>
        <ToastContainer />
        <Loader active={this.state.loading}/>


          <Card style={{width: "80%", margin: "0 auto"}}>
        <CardBody >
        <CardTitle>Survey Title</CardTitle>
        <Survey.Survey model={model}/>
        </CardBody>
        </Card>
      

      </Container>
    );
  }
}

export default SurveyResponse;