/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import * as Survey from "survey-react";
import "survey-react/survey.css";
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { css } from 'glamor'
import config from './config.json'
import cuid from 'cuid';
import store from 'store'
import { Dimmer, Loader } from 'semantic-ui-react'
import { Transfer, Table } from 'antd'
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Badge,
  Button,
  Form, FormInput, FormGroup, FormCheckbox, Modal, ModalHeader, ModalBody, Alert
} from "shards-react";

import PageTitle from "../components/common/PageTitle";
import 'antd/dist/antd.css';
import difference from 'lodash/difference';
const leftTableColumns = [
  {
    dataIndex: 'name',
    title: 'Name',
  },
  {
    dataIndex: 'title',
    title: 'Title',
  },
  {
    dataIndex: 'group',
    title: 'Group'
  }
];
const rightTableColumns = [
  {
    dataIndex: 'name',
    title: 'Name',
  },
];
var widget = {
  //the widget name. It should be unique and written in lowcase.
  name: "transfer",
  //the widget title. It is how it will appear on the toolbox of the SurveyJS Editor/Builder
  title: "transfer",
  //the name of the icon on the toolbox. We will leave it empty to use the standard one
  iconName: "",
  //If the widgets depends on third-party library(s) then here you may check if this library(s) is loaded
  widgetIsLoaded: function () {
      //return typeof $ == "function" && !!$.fn.select2; //return true if jQuery and select2 widget are loaded on the page
      return true; //we do not require anything so we just return true. 
  },
  //SurveyJS library calls this function for every question to check, if it should use this widget instead of default rendering/behavior
  isFit: function (question) {
      //we return true if the type of question is textwithbutton
      return question.getType() === 'transfer';
      //the following code will activate the widget for a text question with inputType equals to date
      //return question.getType() === 'text' && question.inputType === "date";
  },
  //Use this function to create a new class or add new properties or remove unneeded properties from your widget
  //activatedBy tells how your widget has been activated by: property, type or customType
  //property - it means that it will activated if a property of the existing question type is set to particular value, for example inputType = "date" 
  //type - you are changing the behaviour of entire question type. For example render radiogroup question differently, have a fancy radio buttons
  //customType - you are creating a new type, like in our example "textwithbutton"
  activatedByChanged: function (activatedBy) {
      //we do not need to check acticatedBy parameter, since we will use our widget for customType only
      //We are creating a new class and derived it from text question type. It means that text model (properties and fuctions) will be available to us
      // Survey.JsonObject.metaData.addClass("textwithbutton", [], null, "text");
      //signaturepad is derived from "empty" class - basic question class
      //Survey.JsonObject.metaData.addClass("signaturepad", [], null, "empty");

      //Add new property(s)
      //For more information go to https://surveyjs.io/Examples/Builder/?id=addproperties#content-docs
      // Survey.JsonObject.metaData.addProperties("textwithbutton", [
      //     { name: "buttonText", default: "Click Me" }
      // ]);
  },
  //If you want to use the default question rendering then set this property to true. We do not need any default rendering, we will use our our htmlTemplate
  isDefaultRender: false,
  //You should use it if your set the isDefaultRender to false
  htmlTemplate: "<div><TableTransfer dataSource={[{ \
key: '0', \
name: `Mauricio Zamora`, \
title: 'CEO', \
group: 'CEO'}, { \
key: '1', \
name: `Salvador Garcia`,\
title: 'VP',\
group: 'Finance'}, {\
key: '2',\
name: `Chasity Moreno`,\
title: 'Director',\
group: 'R&D'}]}\
          targetKeys={this.state.targetKeys}\
          onChange={this.transferOnChange}\
          filterOption={(inputValue, item) =>\
            item.title.indexOf(inputValue) !== -1\
          }\
          leftColumns={leftTableColumns}\
          rightColumns={rightTableColumns}\
        /></div>",
  //The main function, rendering and two-way binding
  afterRender: function (question, el) {
      //el is our root element in htmlTemplate, is "div" in our case
      //get the text element


  },
  //Use it to destroy the widget. It is typically needed by jQuery widgets
  willUnmount: function (question, el) {
      //We do not need to clear anything in our simple example
      //Here is the example to destroy the image picker
      //var $el = $(el).find("select");
      //$el.data('picker').destroy();
  }
}
// Customize Table Transfer
const TableTransfer = ({ leftColumns, rightColumns, ...restProps }) => (
  <Transfer {...restProps} showSelectAll={false}>
    {({
      direction,
      filteredItems,
      onItemSelectAll,
      onItemSelect,
      selectedKeys: listSelectedKeys,
      disabled: listDisabled,
    }) => {
      const columns = direction === 'left' ? leftColumns : rightColumns;

      const rowSelection = {
        getCheckboxProps: item => ({ disabled: listDisabled || item.disabled }),
        onSelectAll(selected, selectedRows) {
          const treeSelectedKeys = selectedRows
            .filter(item => !item.disabled)
            .map(({ key }) => key);
          const diffKeys = selected
            ? difference(treeSelectedKeys, listSelectedKeys)
            : difference(listSelectedKeys, treeSelectedKeys);
          onItemSelectAll(diffKeys, selected);
        },
        onSelect({ key }, selected) {
          onItemSelect(key, selected);
        },
        selectedRowKeys: listSelectedKeys,
      };

      return (
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={filteredItems}
          size="small"
          style={{ pointerEvents: listDisabled ? 'none' : null }}
          onRow={({ key, disabled: itemDisabled }) => ({
            onClick: () => {
              if (itemDisabled || listDisabled) return;
              onItemSelect(key, !listSelectedKeys.includes(key));
            },
          })}
        />
      );
    }}
  </Transfer>
);
class SurveyBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model:  {
        showProgressBar: "bottom",
        pages: []
      },
      loading: false,
      publishView: false,
       currentQuestion: -1,
       questions: [],
       options: [],
        targetKeys: []
    }
    console.log("constructor")
    const { history } = this.props;
    console.log("visted", store.get("visited"))
    if(store.get("visited") == true){
      console.log("refreshing")
      window.location.reload()
      store.set("visited", false)
    }else{
      store.set("visited", true)

    }
    store.set("email", "test@test.com")
    store.set("surveyid", "123")
  // Survey.SurveyQuestion.renderQuestionBodyOrig = Survey.SurveyQuestion
        const originalthis = this
// console.log("sq", Survey.SurveyQuestion)
const ssq = Survey.SurveyQuestion.renderQuestionBody
Survey.SurveyQuestion.renderQuestionBody = function (creator, question) {
// console.log(creator, question) 
console.log(ssq)
  const questionBody = ssq(creator, question);
  console.log(questionBody)
  

    var deleteButton = async (e) =>{
      console.log("delete entered")
      await originalthis.deleteButton(e)
    }
    var editButton = async (e) =>{
      console.log("edit entered", originalthis)
      await originalthis.editButton(e)
    }
    var moveUpButton = async (e) =>{
      console.log("moveup entered", originalthis)
      await originalthis.moveUpButton(e)
    }
    var moveDownButton = async (e) =>{
      console.log("movedown entered", originalthis)
      await originalthis.moveDownButton(e)
    }
    if(questionBody.props == undefined || questionBody.props.children == undefined ||questionBody.props.children.length == 1){

    console.log("true")
    let newkey =  (new Date().getUTCMilliseconds()).toString() + Math.ceil(Math.random() * 9).toString()
    return (<div key={"d21-" + newkey}>{questionBody}<Button key={"b1-" + newkey} outline type="button" onClick={()=>deleteButton(question)}>Delete</Button><Button outline type="button" key={"b2-" + newkey} onClick={()=>editButton(question)}>Edit</Button><Button outline key={"b3-" + newkey} type="button" onClick={()=>moveUpButton(question)}>↑</Button><Button outline key={"b4-" + newkey} type="button" onClick={()=>moveDownButton(question)}>↓</Button></div>)
    }
    else{
      console.log("false")
      return questionBody;
    }
  }
  Survey.CustomWidgetCollection.Instance.addCustomWidget(widget, "transfer");
    
  }

componentDidMount = async () => {
       
      let load_model = await axios.post(config.url + "/get/survey", {
        email: store.get("email"),
        surveyid: store.get("surveyid")
      })
      console.log("loaded model from firestore", load_model)
      if(load_model.data.msg == "success"){
        this.setState({model: load_model.data.model["saved_model"]})
      }
    
 
    
}
 handleChange = (nextTargetKeys, direction, moveKeys) => {
    this.setState({ targetKeys: nextTargetKeys });

    console.log('targetKeys: ', nextTargetKeys);
    console.log('direction: ', direction);
    console.log('moveKeys: ', moveKeys);
  };

  handleSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    this.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });

    console.log('sourceSelectedKeys: ', sourceSelectedKeys);
    console.log('targetSelectedKeys: ', targetSelectedKeys);
  };

  handleScroll = (direction, e) => {
    console.log('direction:', direction);
    console.log('target:', e.target);
  };

editTab =  () => {
  console.log("currentQuestion", this.state.currentQuestion)
  let num = this.state.currentQuestion
  console.log(num, this.state.model.elements)
  let curr_q = this.state.currentQuestion == -1 || num == NaN || this.state.model.elements == undefined || this.state.model.elements.length == 0? null : this.state.model.elements[num].type
  switch(curr_q){
    case 'text':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
      </FormGroup>
      <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      <FormGroup>
        <label> Placeholder</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handlePlaceholderChange(e)}} placeholder={this.state.model.elements[num].placeHolder}/>
      </FormGroup>
    </Form>)
    case 'comment':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      </FormGroup>
      <FormGroup>
        <label> Placeholder</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handlePlaceholderChange(e)}} placeholder={this.state.model.elements[num].placeHolder}/>
      </FormGroup>
    </Form>)
    case 'radiogroup':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      </FormGroup>
      <FormGroup>
        <label> Options</label>
        {this.state.model.elements[num].choices.map((option, index) =>{
          return (<Row style={{marginLeft: "1%"}}><FormInput  style={{width: "70%"}} key={"option_" + index.toString()} value={option} onChange={(e) => {this.handleOptionsChange(e, index)}}/><Button outline  theme="danger" key={"option_remove_" + index.toString()} type="button" onClick={(e)=>{this.handleRemoveOption(e, index)}}>
        X
      </Button></Row>)
        })}
      </FormGroup>
      <Button outline theme="secondary" type="button" onClick={(e)=>{this.handleAddOption(e)}}>
        Add Option
      </Button>
    </Form>)
    case 'checkbox':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      </FormGroup>
      <FormGroup>
        <label> Options</label>
        {this.state.model.elements[num].choices.map((option, index) =>{
          return (<Row style={{marginLeft: "1%"}}><FormInput style={{width: "70%"}} key={"option_check" + index.toString()} value={option} onChange={(e) => {this.handleOptionsChange(e, index)}}/><Button outline theme="danger" key={"option_remove_" + index.toString()} type="button" onClick={(e)=>{this.handleRemoveOption(e, index)}}>
        X
      </Button></Row>)
        })}
      </FormGroup>
      <Button outline theme="secondary" type="button" onClick={(e)=>{this.handleAddOption(e)}}>
        Add Option
      </Button>
    </Form>)
    case 'rating':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      <label>Minimum Rating Description</label>
      <FormInput style={{width: "85%"}} onChange={(e) => {this.handleMinChange(e)}} value={this.state.model.elements[num].minRateDescription}/>
      <label>Maximum Rating Description</label>
      <FormInput style={{width: "85%"}} onChange={(e) => {this.handleMaxChange(e)}} value={this.state.model.elements[num].maxRateDescription}/>
      </FormGroup>

    </Form>)
    case 'dropdown':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      </FormGroup>
      <FormGroup>
        <label> Options</label>
        {this.state.model.elements[num].choices.map((option, index) =>{
          return (<Row style={{marginLeft: "1%"}}><FormInput style={{width: "70%"}}  key={"option_drop" + index.toString()} value={option} onChange={(e) => {this.handleOptionsChange(e, index)}}/><Button outline theme="danger" key={"option_remove_" + index.toString()} type="button" onClick={(e)=>{this.handleRemoveOption(e, index)}}>
        X
      </Button></Row>)
        })}
      </FormGroup>
      <Button outline theme="secondary" type="button" onClick={(e)=>{this.handleAddOption(e)}}>
        Add Option
      </Button>
    </Form>)
    case 'matrix':
      return (<Form>
      <FormGroup>
        <label> Question</label>
        <FormInput style={{width: "85%"}} onChange={(e) => {this.handleQuestionChange(e)}} value={this.state.model.elements[num].title}/>
        <FormCheckbox checked={this.state.model.elements[num].isRequired} onChange={(e) => this.handleRequired(e)}>
      Required
      </FormCheckbox>
      <FormCheckbox checked={this.state.model.elements[num].isAllRowRequired} onChange={(e) => this.handleRowsRequired(e)}>
      Require all rows to be answered
      </FormCheckbox>
      </FormGroup>
      <FormGroup>
        <label> Column Title</label>
        {this.state.model.elements[num].columns.map((option, index) =>{
          return (<Row style={{marginLeft: "1%"}}><FormInput style={{width: "70%"}}  key={"column_" + index.toString()} value={option.text} onChange={(e) => {this.handleMatrixChangeCol(e, index)}}/><Button outline theme="danger" key={"column_remove_" + index.toString()} type="button" onClick={(e)=>{this.handleRemoveMatrixCol(e, index)}}>
        X
      </Button></Row>)
          
        })
      }
        <Button outline theme="secondary" type="button" onClick={(e)=>{this.handleAddMatrixCol(e)}}>
        Add Column
      </Button>
      <br></br>
        <label> Row Title</label>
        {this.state.model.elements[num].rows.map((option, index) =>{
          return (<Row style={{marginLeft: "1%"}}><FormInput style={{width: "70%"}}  key={"row_" + index.toString()} value={option.text} onChange={(e) => {this.handleMatrixChangeRow(e, index)}}/><Button outline theme="danger" key={"row_remove_" + index.toString()} type="button" onClick={(e)=>{this.handleRemoveMatrixRow(e, index)}}>
        X
      </Button></Row>)
        })}
        <Button outline theme="secondary" type="button" onClick={(e)=>{this.handleAddMatrixRow(e)}}>
        Add Row
      </Button>
      </FormGroup>
      
    </Form>)
    default: 
      return null;

  }
}
handleQuestionChange = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].title = e.target.value

  await this.setState({model: this.state.model})
}
handleMaxChange = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].maxRateDescription = e.target.value

  await this.setState({model: this.state.model})
}
handleMinChange = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].minRateDescription = e.target.value

  await this.setState({model: this.state.model})
}
handleRequired = async (e) =>{
  this.state.model.elements[this.state.currentQuestion].isRequired = !this.state.model.elements[this.state.currentQuestion].isRequired
  await this.setState({model: this.state.model})
}
handleRowsRequired = async (e) =>{
  this.state.model.elements[this.state.currentQuestion].isAllRowRequired = !this.state.model.elements[this.state.currentQuestion].isAllRowRequired
  await this.setState({model: this.state.model})
}
handlePlaceholderChange = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].placeHolder = e.target.value

  await this.setState({model: this.state.model})
}
handleOptionsChange = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].choices[index] = e.target.value

  await this.setState({model: this.state.model})
}
handleAddOption = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].choices.push('New Option')

  await this.setState({model: this.state.model})
}
handleRemoveOption = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].choices.splice(index, 1)

  await this.setState({model: this.state.model})
}

//Matrix handling
handleMatrixChangeRow = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].rows[index].text = e.target.value
    this.state.model.elements[this.state.currentQuestion].rows[index].value = e.target.value

  await this.setState({model: this.state.model})
}
handleAddMatrixRow = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].rows.push({text: 'New Option', value: 'New Option'})

  await this.setState({model: this.state.model})
}
handleRemoveMatrixRow = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].rows.splice(index, 1)

  await this.setState({model: this.state.model})
}

handleMatrixChangeCol = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].columns[index].text = e.target.value
    this.state.model.elements[this.state.currentQuestion].columns[index].value = e.target.value

  await this.setState({model: this.state.model})
}
handleAddMatrixCol = async (e) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].columns.push({text: 'New Option', value: 'New Option'})

  await this.setState({model: this.state.model})
}
handleRemoveMatrixCol = async (e, index) =>{
  console.log("E", this.state.model.elements[this.state.currentQuestion])
    this.state.model.elements[this.state.currentQuestion].columns.splice(index, 1)

  await this.setState({model: this.state.model})
}

transferOnChange = nextTargetKeys => {
    this.setState({ targetKeys: nextTargetKeys });
  };
SQL = async () => {
           console.log("in SQL")

           let elements = this.state.model.pages == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({ type: "text", title: "Question Text", isRequired: false, name: elements.length.toString(), question_id: cuid()})
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
MQL = async () => {
           console.log("in MQL")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({ type: "comment", title: "Question Text", isRequired: false, name: elements.length.toString(), question_id: cuid()})
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Radio = async () => {
           console.log("in Radio")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "radiogroup",
            title: "Question Text",
            colCount: 2,
            isRequired: false,
            choices: ["A", "B"],
            name: elements.length.toString(),
            question_id: cuid()
        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Checkbox = async () => {
           console.log("in Checkbox")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "checkbox",
            title: "Question Text",
            colCount: 2,
            isRequired: false,
            choices: ["A", "B"],
            name: elements.length.toString(),
            question_id: cuid()

        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Dropdown = async () => {
           console.log("in Dropdown")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "dropdown",
            title: "Question Text",
            colCount: 2,
            isRequired: false,
            choices: ["A", "B"],
            name: elements.length.toString(),
            question_id: cuid()

        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Matrix = async () => {
           console.log("in Matrix")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "matrix",
            title: "Question Text",
            columns: [{value: "Header 1", text: "Header 1"}, {value: "Header 1", text: "Header 2"}],
            isRequired: false,
            rows: [{value: "Response 1", text: "Response 1"}, {value: "Response 2", text: "Response 2"}],
            name: elements.length.toString(),
            isAllRowRequired: false,
            question_id: cuid()

        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Rating = async () => {
           console.log("in Rating")

           let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "rating",
            title: "Question Text",
            minRateDescription: "Min Rating Description",
            maxRateDescription: "Max Rating Description",
            isRequired: false,
            name: elements.length.toString(),
            question_id: cuid()

        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})
           console.log("updated", this.state.model.elements)
        };
Transfer = async () =>{
    console.log("in transfer")
    let elements = this.state.model.elements == null || this.state.model.elements == undefined ? [] : this.state.model.elements
           elements.push({
            type: "transfer",
            title: "Question Text",
            targetKeys: [],
            maxRateDescription: "Max Rating Description",
            isRequired: false,
            name: elements.length.toString(),
            question_id: cuid()

        })
           // elements.push({type:"customSQL", title: "test"})

           await this.setState({model: {"elements": elements}, currentQuestion: elements.length - 1})

}
editButton = async (e) =>{
  console.log("edit clicked", e)
  if(this.state.currentQuestion != parseInt(e.name)){
    await this.setState({currentQuestion: parseInt(e.name)})
  }
  
}
deleteButton = async (e) =>{
  console.log("delete clicked", e)
  let num = parseInt(e.name)
  console.log(e.name)
  this.state.model.elements.splice(num, 1)
  for(let i = 0; i < this.state.model.elements.length; i++){
    this.state.model.elements[i].name = i.toString()
  }
  console.log("num elements", this.state.model.elements.length)
  if(this.state.model.elements.length == 0 || this.state.currentQuestion <= 0){
    console.log("foo")
    await this.setState({currentQuestion: 0, model: this.state.model})
  }
  else{
    await this.setState({ currentQuestion: this.state.currentQuestion - 1, model: this.state.model})
  }
  
}
moveUpButton = async (e) => {
  let num = parseInt(e.name)
  let above = num - 1
  if(above >= 0){
    let above_item = this.state.model.elements[above]
    let original = this.state.model.elements[num]
    this.state.model.elements[above] = original
    this.state.model.elements[num] = above_item
    for(let i = 0; i < this.state.model.elements.length; i++){
    this.state.model.elements[i].name = i.toString()
  }
    await this.setState({ model: this.state.model})
  }
}

moveDownButton = async (e) => {
  let num = parseInt(e.name)
  let below = num + 1
  if(below < this.state.model.elements.length){
    let below_item = this.state.model.elements[below]
    let original = this.state.model.elements[num]
    this.state.model.elements[below] = original
    this.state.model.elements[num] = below_item
    for(let i = 0; i < this.state.model.elements.length; i++){
    this.state.model.elements[i].name = i.toString()
  }
    await this.setState({ model: this.state.model})
  }
}
togglePublishModal = async () =>{
  console.log("toggling")
  await this.setState({publishView: !this.state.publishView})
}
publish = async (e) => {
  console.log("publishing")
  this.setState({loading: true})
  let response = await axios.post(config.url + '/surveys/publish', {
    model: this.state.model,
    surveyid: store.get("surveyid"),
    email: store.get("email")
  })
  let response2 = await axios.post(config.url + "/surveys/save",{
    surveyid: store.get("surveyid"),
    model: this.state.model,
    email: store.get("email")
  })
  console.log(response)
  if(response.data.msg == 'success'){
    console.log("success")
    alert('Survey Published!')

  }else{
    alert('An Error Occurred')
  }
  this.setState({loading: false})
  this.togglePublishModal()

}
toggleSave = async () =>{
  this.setState({loading: true})
  let response = await axios.post(config.url + "/surveys/save",{
    surveyid: store.get("surveyid"),
    model: this.state.model,
    email: store.get("email")
  })
  // test
  if(response.data.msg == 'success'){
    alert('Survey Saved!')
  }else{
    alert('An Error Occurred')
  }
  this.setState({loading: false})
}
  render() {
    const {
      PostsListOne,
      PostsListTwo,
      PostsListThree,
      PostsListFour
    } = this.state;

    var model = new Survey.Model(this.state.model);
    Survey.surveyStrings.emptySurvey = "The current survey is empty";
    var edit_tab =  this.editTab()
   
    // if(Object.keys(this.state.model).length > 0){
    //    console.log("local storage set", this.state.model)
    //   localStorage.setItem("model", JSON.stringify(this.state.model))

    // }

    // model
    // .onUpdateQuestionCssClasses
    // .add(function (survey, options) {
    //     var classes = options.cssClasses
    //     console.log("ON UPDATE")
    //     console.log(options)
    //     let question = document.getElementById(options.question.id)
    //     console.log("qeustion", options.question.id, question)
    // });
    return (

      <Container fluid className="main-content-container px-4" >
        {/* Page Header */}
      
      <Loader active={this.state.loading}/>


        <Row noGutters className="page-header py-4" style={{width: "100%", clear: "both", margin: "0  auto"}}>
          <PageTitle sm="4" className="text-sm-left" />
          <Row style={{float: "right", margin: "0 auto", marginRight: "-5px"}} >
          <Button size="lg" onClick={this.toggleSave}>Save</Button><Button size="lg" onClick={this.togglePublishModal}>Publish</Button>
          </Row>
        </Row>
        <ToastContainer />
        <Modal open={this.state.publishView} toggle={this.togglePublishModal}>
          <ModalHeader>Are you sure you want to publish?</ModalHeader>
          <br></br>
          <Button  theme="success" onClick={this.publish}>Confirm</Button>
          <Button  theme="danger" onClick={this.togglePublishModal}>Cancel</Button>
          
        </Modal>
        <Row >
        <Card style={{width: "61%"}}>
        
        <CardBody >
        <CardTitle>Add a Question</CardTitle>
        
        <Button outline onClick={this.MQL}>Text</Button>
        <Button outline onClick={this.Radio}>Multiple Choice</Button>
        <Button outline onClick={this.Checkbox}>Checkbox</Button>
        <Button outline onClick={this.Matrix}>Matrix</Button>

        </CardBody>
        </Card>

          <Card style={{width: "61%"}}>
        <CardBody >
        <CardTitle >Preview</CardTitle>
        <Survey.Survey model={model} />
        <TableTransfer
          dataSource={[{
    key: '0',
    name: `Mauricio Zamora`,
    title: 'CEO',
    group: 'CEO'}, {
    key: '1',
    name: `Salvador Garcia`,
    title: 'VP',
    group: 'Finance'}, {
    key: '2',
    name: `Chasity Moreno`,
    title: 'Director',
    group: 'R&D'}]}
          targetKeys={this.state.targetKeys}
          onChange={this.transferOnChange}
          filterOption={(inputValue, item) =>
            item.title.indexOf(inputValue) !== -1
          }
          leftColumns={leftTableColumns}
          rightColumns={rightTableColumns}
        />

        </CardBody>
        </Card>
      
        <Card style={{width: "39%", position: "fixed", left: "68%"}}>
        <CardBody>
        <CardTitle>Edit Question</CardTitle>
        {edit_tab}
        </CardBody>
        </Card>

      

        </Row>
        {/* First Row of Posts */}
      
      </Container>
    );
  }
}

export default SurveyBuilder;
