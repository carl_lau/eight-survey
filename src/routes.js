import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, NoNavDefaultLayout } from "./layouts";
import store from 'store'
// Route Views
import SurveyBuilder from "./views/SurveyBuilder";
import SurveyConsumer from "./views/SurveyConsumer";
import isLoggedIn from './utils/store';
export default [
    {
    path: "/edit",
    layout: NoNavDefaultLayout,
    component: SurveyBuilder 

  },
    {
    path: "/023ur23jfws0j1rq",
    layout: NoNavDefaultLayout,
    component: SurveyConsumer,
    auth: false
  }
];
